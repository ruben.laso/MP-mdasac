\section{Problema de capas anidadas}
Se propone el siguiente problema
\begin{equation} \label{eq:capas-anidadas}
	\begin{cases}
	\epsilon^3 x y'' + x^2 y' - (x^3 + \epsilon)y = 0, \quad & 0 < x < 1,
	\\
	y(0) = 1, y(1) = \sqrt{\epsilon}, \quad & 0 < \epsilon \ll 1.
	\end{cases}
\end{equation}

Suponemos que no existe capa límite en $x=1$ y proponemos un desarrollo exterior, igual que el de los problemas anteriores, con el que obtenemos
\begin{align*}
	\bigo{1} &\implies y_0(x) = e^{\frac{x^2}{2}},
	\\
	\bigo{\epsilon} &\implies y_1(x) = \left(1 - \frac{1}{x}\right) e^{\frac{x^2}{2}},
	\\
	\vdots
\end{align*}
Debido a que $\lim\limits_{x\rightarrow0^+}y_1(x) = -\infty$ diverge, sospechamos que existe una capa límite en $x=0$ de anchura $\delta(\epsilon)$.

Proponemos el cambio de variable $X = \frac{x}{\delta}$ para calcular el desarrollo interior y nos queda la ecuación
\begin{equation*}
	\frac{\epsilon^3 X}{\delta}Y_{XX} + \delta X^2 Y_X - \delta^3 X^3 Y - \epsilon Y = 0,
\end{equation*}
de donde obtenemos los siguientes balances consistentes:
\begin{itemize}
	\item Balance $\delta X^2 Y_X \sim \delta^3 X^3 Y$ y dominan:
	\begin{equation*}
		\delta = \delta^3 \implies \delta(\epsilon) = 1,
	\end{equation*}
	donde obtenemos el límite exterior.
	
	\item Balance $\delta X^2 Y_X \sim \epsilon Y$ y dominan:
	\begin{equation*}
		\delta(\epsilon) = \epsilon.
	\end{equation*}
	
	\item Balance $\frac{\epsilon^3 X}{\delta} \sim \epsilon Y$ y dominan:
	\begin{equation*}
		\delta(\epsilon) = \epsilon^2.
	\end{equation*}
\end{itemize}
Con esto, deducimos que hay dos capas límite alrededor de $x=0$, una de tamaño $\epsilon$ y otra de espesor $\epsilon^2$.

Comenzamos resolviendo la capa de tamaño $\delta(\epsilon) = \epsilon$, con $X = \frac{x}{\epsilon}$:
\begin{align*}
	\bigo{1} &\implies Y_0(x) = c_0 e^{-\frac{1}{X}},
	\\
	\bigo{\epsilon} &\implies Y_1(x) = c_1 e^{-\frac{1}{X}} + c_0\left[\frac{2}{3X^3} - \frac{1}{4X^4}\right] e^{-\frac{1}{X}},
	\\
	\vdots
\end{align*}
donde, por límites, llegamos a que $c_0 = c_1 = 1$, por lo que
\begin{align*}
	Y_0(x) &= e^{\frac{\epsilon}{x}},
	\\
	Y_1(x) &= e^{\frac{\epsilon}{x}} \left[1 + \frac{2\epsilon^3}{3x^3} - \frac{\epsilon^4}{4x^4}\right],
	\\
	\vdots
\end{align*}

Escalamos ahora $\hat{X} = \frac{x}{\epsilon^2}$ y llegamos a la EDO
\begin{equation*}
	\hat{X}\hat{Y}_{\hat{X}\hat{X}}
	+
	\epsilon \hat{X}^2 \hat{Y}_{\hat{X}}
	-
	\left(1 + \epsilon^5\right)\hat{X}^3\hat{Y}
	=
	0,
\end{equation*}
de la cual obtenemos que la solución de orden principal es 
\begin{equation*}
	\hat{Y}_0(x) = \alpha\sqrt{\hat{X}} I_1\left(2\sqrt{\hat{X}}\right) + \beta\sqrt{\hat{X}}K_1\left(2\sqrt{\hat{X}}\right),
\end{equation*}
donde $I_1, K_1$ son funciones de Bessel modificadas y que $\alpha = 0, \beta=2$.
Por lo que tenemos que la solución uniforme \textit{leading order} es:
\begin{equation*}
y_{\text{unif}} (x; \epsilon) = y_0(x) + Y_0(x) + \hat{Y}_0(x) - \mathcal{C}_0(x) = e^{\frac{x^2}{2}} + e^{-\frac{\epsilon}{x}} + 2\sqrt{\frac{x}{\epsilon^2}}K_1\left(2\sqrt{\frac{x}{\epsilon^2}}\right) - \mathcal{C}_0(x).
\end{equation*}
Para hallar $\mathcal{C}_0(x)$, suponemos $x = \bigo{1}; \epsilon\rightarrow0^+$:
\begin{equation*}
	\lim\limits_{\substack{\epsilon \rightarrow 0^+\\x = \bigo{1}}} y_{\text{unif}} (x; \epsilon)
	= e^{\frac{1}{2}} + 1 + 0 - \mathcal{C}_0(x) = \lito{1} \implies \mathcal{C}_0(x) = e^{\frac{1}{2}} + 1.
\end{equation*}

Así pues, la solución \textit{leading order} es
\begin{equation} \label{eq:capas-anidadas-lo}
	y_{\text{unif}} (x; \epsilon)
	=
	e^{\frac{x^2}{2}} 
	+ 
	e^{-\frac{\epsilon}{x}} 
	+ 
	2\sqrt{\frac{x}{\epsilon^2}}K_1\left(2\sqrt{\frac{x}{\epsilon^2}}\right) 
	-
	e^{\frac{1}{2}} - 1.
\end{equation}

\subsection{Estudio de las soluciones asintóticas}
En este caso sólo se ha obtenido una solución asintótica, la \textit{leading order} mostrada en la Expresión~\eqref{eq:capas-anidadas-lo}, puesto que era complejo obtener la \textit{first order}. Para el problema~\eqref{eq:capas-anidadas} se ha obtenido la solución numérica con el comando \texttt{bvpc4} de MATLAB y se han utilizado los valores de $\epsilon = \{0.05, 0.1, 0.5\}$. En la \figurename{~\ref{fig:capas-anidadas}} se recogen los resultados obtenidos.

\begin{figure}[htbp]
	\centering
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/capas-anidadas/lo_005}
		\caption{\textit{Leading order}, $\epsilon = 0.05$.}
		\label{fig:capas-anidadas-lo-001}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/capas-anidadas/lo_01}
		\caption{\textit{Leading order}, $\epsilon = 0.1$.}
		\label{fig:capas-anidadas-lo-005}
	\end{subfigure}%
	\\
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/capas-anidadas/lo_05}
		\caption{\textit{Leading order}, $\epsilon = 0.5$.}
		\label{fig:capas-anidadas-lo-01}
	\end{subfigure}%
	\caption{Comparativa de la solución exacta y las aproximaciones asintóticas para diferentes valores de $\epsilon$. Con líneas punteadas verticales se denotan los límites de las capas límite.}
	\label{fig:capas-anidadas}
\end{figure}

En estas figuras vemos cómo las soluciones, para valores de $\epsilon$ menores que 0.5 se diferencian de la solución real una vez fuera de la capa límite debido a la contribución de $Y_0(x)$. Esto nos sugiere que el elemento $\mathcal{C}_0(x)$ no está bien calculado para equilibrar la ecuación, pues las soluciones exterior e interior sí ajustan con precisión la función en sus respectivas zonas. Así mismo, el ajuste dentro de la capa límite tampoco es tan bueno como el visto en otros problemas.

El error máximo de la aproximación tampoco  es el mejor posible, pues alcanza su mínimo en $\epsilon \approx 0.23$. Hasta que $\epsilon$ alcanza ese valor el error decrece de forma prácticamente lineal, y una vez alcanzada esa cota se estanca en el 35\%, una cifra bastante mala.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/capas-anidadas/error}
	\caption{Error máximo relativo cometido en las aproximaciones.}
	\label{fig:capas-anidadas-error}
\end{figure}

Con estos datos, podemos afirmar que la aproximación asintótica de la solución no ha sido todo lo buena que desearíamos. Sin embargo, es de esperar que al obtener más términos en los desarrollos asintóticos mejoraríamos sustancialmente la precisión en la solución obtenida.