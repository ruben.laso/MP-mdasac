\section{Problema de contorno singular}
En esta sección se plantea el problema
\begin{equation} \label{eq:contorno-singular}
	\begin{cases}
	\epsilon y'' + \frac{1}{x}y' + y = 0, \quad & 0 < x < 1,
	\\
	y(0) = \alpha,\quad y(1)=e^{-\frac{1}{2}}, \quad & 0 < \epsilon \ll 1.
	\end{cases}
\end{equation}

Como $x=0$ es un punto singular de~\eqref{eq:contorno-singular}, podemos sospechar que existe una capa límite en este punto. Para comprobarlo, realizamos un análisis mediante balances dominantes con
\begin{gather*}
	x = \bigo{\delta(\epsilon)}
	\implies
	x = \delta X
	\implies
	X = \frac{x}{\delta},
	\\
	\frac{\epsilon}{\delta^2}Y_{XX} + \frac{1}{\delta^2}\frac{1}{X^2} Y_X + Y = 0.
\end{gather*}
\begin{itemize}
	\item Balance $\frac{\epsilon}{\delta^2} \sim \frac{1}{\delta^2}$ y dominan:
	\begin{equation*}
		\frac{\epsilon}{\delta^2} = \frac{1}{\delta^2} \implies \epsilon = 1.
	\end{equation*}
	Inconsistente, pues las condiciones marcan $\epsilon \ll 1$.
	
	\item Balance $\frac{1}{\delta^2} \sim 1$ y dominan:
	\begin{equation*}
		\frac{1}{\delta^2} = 1 \implies \delta(\epsilon) = 1.
	\end{equation*}
	Consistente, hemos hallado el límite exterior.
	
	\item Balance $\frac{\epsilon}{\delta^2} \sim 1$ y dominan:
	\begin{equation*}
		\frac{\epsilon}{\delta^2} = 1 \implies \delta(\epsilon) = \sqrt{\epsilon} \implies Y_{XX} + \frac{1}{\epsilon}\frac{1}{X} Y_X + Y = 0.
	\end{equation*}
	Inconsistente, domina el segundo término.
\end{itemize}
Podemos concluir que no existe capa límite en $x=0$, por lo que intentamos hacer solo un desarrollo exterior para aproximar la solución.

Suponemos una solución de la forma
\begin{equation} \label{eq:contorno-singular-out}
	y_{\text{out}}(x;\epsilon)
	=
	\sum_{n = 0}^{\infty} y_n(x) \epsilon^n
	=
	y_0(x) + y_1(x)\epsilon + y_2(x)\epsilon^2 + \dots
\end{equation}
y la sustituimos en~\eqref{eq:contorno-singular}, donde podemos separar los términos de mismo orden
\begin{align*}
	\bigo{1}
	&\implies
	\begin{cases}
	\frac{1}{x}y_0' +y_0 = 0
	\\
	y_0(x=1) = y(x=1) = e^{-\frac{1}{2}}
	\end{cases}
	\implies
	y_0(x) = e^{-\frac{x^2}{2}},
	\\
	\bigo{\epsilon}
	&\implies
	\begin{cases}
	\frac{1}{x}y_1' + y_1 = -4e^{-\frac{x^2}{2}}
	\\
	y_1(1) = 0
	\end{cases}
	\implies y_1(x) = -\frac{1}{4}(x^2 - 1)^2 e^{-\frac{x^2}{2}}.
\end{align*}
Así pues, llegamos a que la aproximación \textit{leading order} es:
\begin{equation} \label{eq:contorno-singular-lo}
	y_{\text{unif}}(x;\epsilon) = e^{-\frac{x^2}{2}},
\end{equation}
y la \textit{first order}:
\begin{equation} \label{eq:contorno-singular-fo}
	y_{\text{unif}}(x;\epsilon) = e^{-\frac{x^2}{2}} - -\frac{\epsilon}{4}(x^2 - 1)^2 e^{-\frac{x^2}{2}},
\end{equation}
donde hemos de destacar que el valor de $\alpha$ queda indeterminado, por ello suponemos
\begin{equation*}
	y(0) \sim 1 - \frac{\epsilon}{4} + \bigo{\epsilon^2} \sim \alpha.
\end{equation*}

\subsection{Estudio de las soluciones asintóticas}
Para este problema, a falta de expresión analítica, se ha calculado la solución numérica utilizando el comando \texttt{bvp4c} de MATLAB. .
Las soluciones mostradas en la \figurename{~\ref{fig:contorno-singular}} se han obtenido con los valores $\epsilon = \{0.05, 0.1, 0.5\}$.
\begin{figure}[htbp]
	\centering
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/contorno-singular/lo_005}
		\caption{\textit{Leading order}, $\epsilon = 0.05$.}
		\label{fig:contorno-singular-lo-005}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/contorno-singular/fo_005}
		\caption{\textit{First order}, $\epsilon = 0.05$.}
		\label{fig:contorno-singular-fo-005}
	\end{subfigure}%
	\\
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/contorno-singular/lo_01}
		\caption{\textit{Leading order}, $\epsilon = 0.1$.}
		\label{fig:contorno-singular-lo-01}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/contorno-singular/fo_01}
		\caption{\textit{First order}, $\epsilon = 0.1$.}
		\label{fig:contorno-singular-fo-01}
	\end{subfigure}%
	\\
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/contorno-singular/lo_05}
		\caption{\textit{Leading order}, $\epsilon = 0.5$.}
		\label{fig:contorno-singular-lo-05}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/contorno-singular/fo_05}
		\caption{\textit{First order}, $\epsilon = 0.5$.}
		\label{fig:contorno-singular-fo-05}
	\end{subfigure}%
	\caption{Comparativa de la solución exacta y las aproximaciones asintóticas para diferentes valores de $\epsilon$. Con líneas punteadas verticales se denotan los límites de las capas límite.}
	\label{fig:contorno-singular}
\end{figure}

En estas figuras se puede ver que la principal diferencia entre ambas aproximaciones es la precisión en el punto $x=0$, debido a que hemos realizado una aproximación de $\alpha$ utilizando la solución \textit{first order}. Así, podemos ver que para la solución \textit{leading order} siempre se cumple $y_{\text{unif}}(0) = 1$, independientemente del valor de $\epsilon$. También es notable la mejora en la precisión al añadir el término de orden $\bigo{\epsilon}$, algo que también podemos corroborar en la \figurename{~\ref{fig:contorno-singular-error}}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/contorno-singular/error}
	\caption{Error máximo relativo cometido en las aproximaciones.}
	\label{fig:contorno-singular-error}
\end{figure}
En la aproximación \textit{leading order} el error crece de forma aproximadamente lineal con $\epsilon$. Sin embargo, para la \textit{first order} se mantiene muy por debajo y su crecimiento es pequeño. Como datos concretos, podemos ofrecer que para $\epsilon = 0.1$ (un valor razonable de $\epsilon$) la solución \textit{leading order} da un error máximo relativo del 2.8\%, mientras que la \textit{first order} se queda en 0.3\%.

Con estos datos podemos corroborar la alta precisión de las soluciones asintóticas incluso cuando se utiliza un único término.