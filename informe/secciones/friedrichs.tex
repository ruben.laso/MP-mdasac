\section{Problema de Friedrichs}
El problema de Friedrichs se define tal que
\begin{equation} \label{eq:friedrichs}
	\begin{cases}
	\epsilon y'' + 2y' - y = 0 & \quad 0 < x < 1,
	\\
	y(0) = 0, \quad y(1) = 1 & \quad 0 < \epsilon \ll 1.
	\end{cases}
\end{equation}
La solución analítica de~\eqref{eq:friedrichs} es
\begin{equation} \label{eq:friedrichs-analitica}
	y(x; \epsilon) = \frac{e^{\lambda^+ x} - e^{\lambda^- x}}{e^{\lambda^+} - e^{\lambda^-}},
\end{equation}
\begin{equation*}
	\begin{cases}
	\lambda^+ = \frac{-1 + \sqrt{1 + \epsilon}}{\epsilon} \quad & \xRightarrow[]{\epsilon \rightarrow 0^+} \quad \lambda^+ \rightarrow \frac{1}{2},
	\\
	\lambda^- = \frac{-1 - \sqrt{1 + \epsilon}}{\epsilon} \quad & \xRightarrow[]{\epsilon \rightarrow 0^+} \quad \lambda^- \rightarrow \frac{-2}{\epsilon}.
	\end{cases}
\end{equation*}

En caso de no tener una solución analítica, es posible obtener un desarrollo asintótico muy próximo a la solución real del problema~\eqref{eq:friedrichs}. Para ello, es necesario conocer que existe una capa límite de espesor $\delta(\epsilon)$ en el punto $x = 0$, puesto que para $0 \leq x \leq \delta = \bigo{\epsilon}$ la solución tiene una gran pendiente y varía rápidamente.

Dada esta situación, se procede a calcular dos desarrollos asintóticos, el correspondiente a la región \emph{exterior} (\textit{outer}) y el de la región \emph{interior} (\textit{inner}). Posteriormente, será necesario \emph{acoplar} (\textit{matching}) estas soluciones en la zona de \emph{solapamiento}, hallando así el desarrollo uniforme.

En primer lugar buscamos las soluciones para el desarrollo exterior, donde $\epsilon \ll x < 1$, donde $y(x; \epsilon) = y_{\text{out}}(x;\epsilon)$. Así pues, definimos:
\begin{equation} \label{eq:friedrichs-yout}
	y_{\text{out}}(x;\epsilon)
	=
	\sum_{n = 0}^{\infty} y_n(x) \epsilon^n
	=
	y_0(x) + y_1(x)\epsilon + y_2(x)\epsilon^2 + \dots
\end{equation}
También tenemos que la condición de contorno es independiente de $\epsilon$, por lo que tenemos $y_{\text{out}}(1) = 1$ y será necesario aplicarla solamente a $y_0(x)$.

Si sustituimos~\eqref{eq:friedrichs-yout} en~\eqref{eq:friedrichs} y ordenamos sus términos se obtiene:
\begin{equation*}
	2y_0' - y_0 + \epsilon(y_0'' + 2y_1' - y_1) + \epsilon^2(y_1'' + 2y_2' - y_2) + \epsilon(\dots) + \dots = \sum_{n = 0}^{\infty}0 \epsilon^n = 0 .
\end{equation*}
Ahora podemos coger los términos de mismo orden para $\epsilon$:
\begin{align*}
	\bigo{1} 
	&\implies
	\begin{cases}
	y_0' - \frac{1}{2}y_0 = 0
	\\
	y_0(1) = 1
	\end{cases}
	\implies
	y_0(x) = e^{\frac{1}{2}(x-1)},
	\\
	\bigo{\epsilon} 
	&\implies
	\begin{cases}
	2y_1' - y_1 = -y_0''
	\\
	y_1(1) = 0
	\end{cases}
	\implies
	y_1(x) = \frac{1-x}{8} e^{\frac{1}{2}(x-1)},
	\\
	\vdots
	\\
	\bigo{\epsilon^n}
	&\implies
	\begin{cases}
	2y_n' - y_n = -y_{n-1}''
	\\
	y_n(1) = 0
	\end{cases}
	\implies
	y_n(x) = \dots
\end{align*}

Para el desarrollo interior se propone un método similar, donde debemos realizar el cambio de variable $X = \frac{x}{\epsilon} = \bigo{1}$ y planteamos el desarrollo asintótico
\begin{equation*}
	Y_{\text{inn}}(X;\epsilon)
	=
	\sum_{n = 0}^{\infty} Y_n(X) \epsilon^n
	=
	Y_0(X) + Y_1(X)\epsilon + Y_2(X)\epsilon^2 + \dots
\end{equation*}
Realizando el mismo procedimiento que para el desarrollo exterior tenemos
\begin{align*}
	\bigo{1} 
	&\implies
	\begin{cases}
	Y_0'' - 2Y_0' &= 0
	\\
	Y_0(0) &= 0
	\end{cases}
	\implies
	Y_0(X) = c_0(1 - e^{2X}),
	\\
	\bigo{\epsilon} 
	&\implies
	\begin{cases}
	Y_1'' - 2Y_1' &= Y_0
	\\
	Y_1(1) &= 0
	\end{cases}
	\implies
	Y_1(X) = c_0\frac{X}{2}(1 + e^{-2X}) + c_1(1 - e^{-2X}),
	\\
	\vdots
	\\
	\bigo{\epsilon^n}
	&\implies
	\begin{cases}
	Y_n'' - 2Y_n' &= Y_{n-1}
	\\
	y_n(0) &= 0
	\end{cases}
	\implies
	Y_n(X) = \dots
\end{align*}
donde $c_0 = e^{-\frac{1}{2}}$ se obtiene tomando el límite
\begin{equation*}
	\lim_{x\rightarrow 0^+} y_0(x) = \lim_{X \rightarrow +\infty}Y_0(X)
\end{equation*}
y $c_1 = \frac{e^{-\frac{1}{2}}}{8}$ se calcula junto al dominio de solapamiento de la solución de primer orden
\begin{equation*}
	y_0(x) + \epsilon y_1(x) - Y_0\left(\frac{x}{\epsilon}\right) - \epsilon Y_1\left(\frac{x}{\epsilon}\right) = \lito{\epsilon}.
\end{equation*}

Para la obtención de la solución uniforme asociada al \textit{leading order} debemos tener
\begin{equation*}
	y_{\text{unif}}(x;\epsilon) \sim y_0(x) + Y_0\left(\frac{x}{\epsilon}\right) - \mathcal{C}_0(x) = e^{\frac{1}{2}(x-1)} + e^{-\frac{1}{2}}\left(1 - e^{-\frac{2x}{\epsilon}}\right) - \mathcal{C}_0(x),
\end{equation*}
donde $\mathcal{C}_0(x) = e^{-\frac{1}{2}}$, por lo que la solución asintótica \textit{leading order} será
\begin{equation} \label{eq:friedrichs-lo}
	y_{\text{unif}}(x;\epsilon) \sim e^{\frac{1}{2}(x-1)} - e^{-\frac{1}{2}}e^{-\frac{2x}{\epsilon}}.
\end{equation}

Para la solución de primer orden, tendremos
\begin{equation*}
	y_{\text{unif}}(x;\epsilon) \sim y_0(x) + \epsilon y_1(x) + Y_0\left(\frac{x}{\epsilon}\right) + \epsilon Y_1\left(\frac{x}{\epsilon}\right) - \mathcal{C}_1(x),
\end{equation*}
con $\mathcal{C}_1(x) = e^{-\frac{1}{2}} \left(1 + \frac{\epsilon}{8} + \frac{x}{2} \right)$, así que obtenemos
\begin{equation} \label{eq:friedrichs-fo}
	y_{\text{unif}}(x;\epsilon) \sim e^{\frac{1}{2}(x-1)}\left[1 + \epsilon\frac{1-x}{8}\right] + e^{-\frac{1}{2}} e^{-\frac{2x}{\epsilon}} \left[\frac{x}{2} - 1 - \frac{\epsilon}{8}\right].
\end{equation}

Por último, podemos definir el dominio de solapamiento:
\begin{equation*}
	I_n \sim \left(\epsilon \ln(\epsilon^{-1}),\, \epsilon^{\frac{n}{n+1}}\right).
\end{equation*}

\subsection{Estudio de las soluciones asintóticas}
Una vez que tenemos las soluciones asintóticas de \textit{leading order} y \textit{first order}, Expresiones~\eqref{eq:friedrichs-lo} y~\eqref{eq:friedrichs-fo}, se puede proceder a ver cuán bien se aproximan a la solución analítica, Expresión~\eqref{eq:friedrichs-analitica}.

En este problema hemos representado las soluciones para $\epsilon = \{0.05, 0.2, 0.5\}$, para valores más pequeños de $\epsilon$ es imposible apreciar a simple vista ningún comportamiento. En la \figurename{~\ref{fig:friedrichs}} se recogen estas gráficas.

\begin{figure}[htbp]
	\centering
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/friedrichs/lo_005}
		\caption{\textit{Leading order}, $\epsilon = 0.05$.}
		\label{fig:friedrichs-lo-005}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/friedrichs/fo_005}
		\caption{\textit{First order}, $\epsilon = 0.05$.}
		\label{fig:friedrichs-fo-005}
	\end{subfigure}%
	\\
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/friedrichs/lo_02}
		\caption{\textit{Leading order}, $\epsilon = 0.2$.}
		\label{fig:friedrichs-lo-02}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/friedrichs/fo_02}
		\caption{\textit{First order}, $\epsilon = 0.2$.}
		\label{fig:friedrichs-fo-02}
	\end{subfigure}%
	\\
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/friedrichs/lo_05}
		\caption{\textit{Leading order}, $\epsilon = 0.5$.}
		\label{fig:friedrichs-lo-05}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/friedrichs/fo_05}
		\caption{\textit{First order}, $\epsilon = 0.5$.}
		\label{fig:friedrichs-fo-05}
	\end{subfigure}%
	\caption{Comparativa de la solución exacta y las aproximaciones asintóticas para diferentes valores de $\epsilon$. Con líneas punteadas verticales se denotan los límites de las capas límite.}
	\label{fig:friedrichs}
\end{figure}

En ellas podemos ver la gran precisión de las soluciones asintóticas, pues las diferencias con la solución analítica son prácticamente inapreciables. Cuanto menor es el valor de $\epsilon$, más delgada es la capa límite  y más cerca de $x=0$ se ha de efectuar el \textit{matching} de soluciones interior y exterior. Al ser esta región de \textit{matching} más pequeña, la zona de mayor error se reduce, por lo que obtendremos soluciones más precisas. 

En cuanto al número de términos que se cogen en las soluciones asintóticas, vemos que la solución interior de primer orden ajusta mejor el comportamiento de la solución analítica, incluso fuera de la zona interior. Así pues, debemos destacar que la precisión de la aproximación es notablemente mejor.

En la \figurename{~\ref{fig:friedrichs-error}} se muestra el error máximo relativo cometido por las distintas aproximaciones a medida que variamos el valor de $\epsilon$.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/friedrichs/error}
	\caption{Error máximo relativo cometido en las aproximaciones.}
	\label{fig:friedrichs-error}
\end{figure}

Aquí podemos ver la enorme diferencia en la precisión obtenida al pasar de la aproximación \textit{leading order} a la \textit{first order}. El término adicional de la \textit{first order approximation} permite acotar el error máximo cometido hasta un 0.66\% incluso con valores muy altos de $\epsilon$, algo absolutamente despreciable. Sin embargo, la solución \textit{leading order} tiene un error máximo del 13\%, una cifra que puede ser notable si buscamos una alta precisión del método. Sobre todo, se ha de destacar que el error crece aproximadamente de forma lineal con el valor de $\epsilon$ para la solución \textit{leading order}, mientras que permanece estable para la \textit{first order}.