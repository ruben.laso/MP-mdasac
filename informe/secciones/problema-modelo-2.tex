\section{Problema modelo 2}
Se propone el siguiente problema
\begin{equation} \label{eq:modelo2}
	\begin{cases}
	(\epsilon + x) y'' + y' = 1, \quad & 0<x<1,
	\\
	y(0) = 0, \ y(1) = 2, \quad & 0 < \epsilon \ll 1.
	\end{cases}
\end{equation}
donde tenemos que la solución analítica es
\begin{equation} \label{eq:modelo2-analitica}
	y(x;\epsilon) = x - \frac{\ln(1 + \frac{x}{\epsilon})}{\ln(\epsilon) - \ln(1+\epsilon)}
\end{equation}
y sabemos que existe una capa límite en $x=0$ y que tiene anchura $\delta(\epsilon) = \bigo{\epsilon}$.

Para el desarrollo exterior utilizamos las mismas técnicas que en el resto de problemas, y llegamos al término de orden principal:
\begin{align*}
	\bigo{1}
	&\implies
	y_0(x) = 1 + x + c_0 \ln(x).
\end{align*}

Para el desarrollo interior realizamos el cambio de variable $X=\frac{x}{\epsilon}$, de forma que obtenemos el término:
\begin{equation} \label{eq:modelo2-inner}
	(1+X) (\nu_0 Y_0'' + \nu_1 Y_1'' + \dots) + (\nu_0 Y_0' + \nu_1 Y_1' + \dots) = \epsilon.
\end{equation}
Para encontrar el término principal tenemos tres posibilidades
\begin{itemize}
	\item Caso $\nu_0 \ll \epsilon$: el lado izquierdo de~\eqref{eq:modelo2-inner} es $\lito{\epsilon}$ y el lado derecho es $\bigo{\epsilon}$, por lo que es inconsistente.
	
	\item Caso $\nu_0 \gg \epsilon$: se puede obtener el término principal consistente
	\begin{equation*}
		(1+X) Y_0'' + Y_0 = 0 \implies Y_0(X) = D_0 \ln(1+ X).
	\end{equation*}
	
	\item Caso $\nu_0 = \epsilon$: se obtiene el término principal consistente
	\begin{equation*}
		(1+X)Y_0'' + Y_0' = 1 \implies Y_0(X) = X + D_0 \ln(1+X).
	\end{equation*}
\end{itemize}
Así pues, tenemos dos posibles soluciones que satisfacen la condición de contorno $y(x) = Y(X=0) = Y_0(0) = 0, \forall D_0 \in \mathbb{R}$.

Para comprobar qué solución es la correcta, tenemos que realizar un balance donde $y_0 - \nu_0 Y_0 = \lito{1}$. Comenzamos por la solución obtenida con $\nu_0 = \epsilon$:
\begin{gather*}
	y_0 - \nu_0 Y_0 = \lito{1} \implies 1 + x + c_0\ln(x) - \epsilon[X + D_0\ln(1+X)] = 
	\\
	1+ x + c_0\ln(x) - \epsilon\left[\frac{x}{\epsilon} + D_0\ln(1+\frac{x}{\epsilon})\right] =
	1+ c_0\ln(x) - \epsilon D_0\ln(1+ \frac{x}{\epsilon}) \ne \lito{1},
\end{gather*}
donde no se puede anular el 1, por lo que debemos descartar esta solución.
Realizamos lo mismo para la solución $\nu_0 \gg \epsilon$.
\begin{gather*}
	y_0 - \nu_0 Y_0 = \lito{1} \implies 1 + x + c_0 \ln(x) - \nu_0 D_0\ln(1+\frac{x}{\epsilon}) =
	\\
	1+x+c_0\ln(x) - \nu_0 D_0 \left[\ln(\epsilon+x) - \ln(\epsilon)\right] =
	\\
	1 + x + c_0 \ln(x) + \nu_0 D_0 \ln(\epsilon) - \nu_0 D_0 \ln(x\left(1+\frac{\epsilon}{x}\right)) = 
	\\
	1 + x + c_0\ln(x) + \nu_0 D_0 \ln(\epsilon) - \nu D_0 \ln(x) - \nu_0 D_0 \frac{\epsilon}{x} + \lito{\frac{\epsilon}{x}} = \lito{1}.
\end{gather*}
Esto se cumple imponiendo $\ln(1+\frac{\epsilon}{x}) \sim \frac{\epsilon}{x} + \lito{\frac{\epsilon}{x}}$, si $\frac{\epsilon}{x} = \lito{1}$ tendremos que $\epsilon \ll x \ll 1$, que será la zona de solapamiento.
Además, debemos tomar $c_0 = 0$ y
\begin{equation}
\nu_0 \ln(\epsilon) D_0 = -1 
\implies
\nu_0 = -\frac{1}{D_0 \ln(\epsilon)},
\end{equation}
donde $D_0 = 1$, por lo que $\nu_0 = -\frac{1}{\ln(\epsilon)}$.

Con esto tenemos que la solución \textit{leading order} sería:
\begin{equation} \label{eq:modelo2-lo}
	y_\text{unif}(x;\epsilon) = x - \frac{1}{\ln(\epsilon)} \ln(1 + \frac{x}{\epsilon}).
\end{equation}

Para los términos de orden superior se realizarían cálculos similares, y llegaríamos a que la solución de primer orden sería:
\begin{equation} \label{eq:modelo2-fo}
	y_\text{unif} (x;\epsilon) = x + \left[-\frac{1}{\ln(\epsilon)} - \frac{\epsilon}{(\ln(\epsilon))^2} + \frac{\epsilon^2}{2(\ln(\epsilon))^2}\right] \ln(1 + \frac{x}{\epsilon}).
\end{equation}


\subsection{Estudio de las soluciones asintóticas}
Para el problema~\eqref{eq:modelo2} se han escogido los valores de $\epsilon = \{0.01, 0.1, 0.5\}$. En la \figurename{~\ref{fig:modelo2}} se recogen las comparativas de la solución analítica, Expresión~\eqref{eq:modelo2-analitica}, y las soluciones \textit{leading order} y \textit{first order}, Expresiones~\eqref{eq:modelo2-lo} y~\eqref{eq:modelo2-fo}.


\begin{figure}[htbp]
	\centering
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/modelo-2/lo_001}
		\caption{\textit{Leading order}, $\epsilon = 0.01$.}
		\label{fig:modelo2-lo-001}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/modelo-2/fo_001}
		\caption{\textit{First order}, $\epsilon = 0.01$.}
		\label{fig:modelo2-fo-001}
	\end{subfigure}%
	\\
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/modelo-2/lo_01}
		\caption{\textit{Leading order}, $\epsilon = 0.1$.}
		\label{fig:modelo2-lo-005}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/modelo-2/fo_01}
		\caption{\textit{First order}, $\epsilon = 0.1$.}
		\label{fig:modelo2-fo-005}
	\end{subfigure}%
	\\
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/modelo-2/lo_05}
		\caption{\textit{Leading order}, $\epsilon = 0.5$.}
		\label{fig:modelo2-lo-05}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/modelo-2/fo_05}
		\caption{\textit{First order}, $\epsilon = 0.5$.}
		\label{fig:modelo-1-fo-05}
	\end{subfigure}%
	\caption{Comparativa de la solución exacta y las aproximaciones asintóticas para diferentes valores de $\epsilon$. Con líneas punteadas verticales se denotan los límites de las capas límite.}
	\label{fig:modelo2}
\end{figure}

En estas gráficas se puede apreciar lo rápido que convergen las soluciones asintóticas hacia la solución analítica a medida que disminuimos el valor de $\epsilon$. En este sentido, también se puede apreciar que la solución \textit{first order} converge más rápido que la \textit{leading order}, pues es mucho más parecida a la solución analítica. De hecho, los errores máximos de la aproximación \textit{first order} disminuyen prácticamente de forma cúbica. En la \figurename{~\ref{fig:modelo2-error}} se recogen estos datos.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/modelo-2/error}
	\caption{Error máximo relativo cometido en las aproximaciones.}
	\label{fig:modelo2-error}
\end{figure}

Se debe notar la sustancial diferencia entre los errores de las soluciones, donde la \textit{first order} se mantiene por debajo del 5\% de error hasta valores de $\epsilon \approx 0.35$, los cuales ya son grandes para este parámetro. En la \textit{leading order} el error crece mucho más a medida que aumentamos $\epsilon$ y rápidamente se sitúa por encima del 5\%. 

Datos reveladores de la precisión de la aproximación \textit{leading order} son que, para $\epsilon = 0.01$ se obtiene un error máximo del $4.3 \cdot 10^{-4}$\%, mientras que con $\epsilon = 0.1$ sólo erramos en un $0.13$\%, algo totalmente despreciable en la mayoría de situaciones. Para las mismas situaciones, la solución \textit{leading order} se queda en un error del $0.2$\% y del 3\% respectivamente.