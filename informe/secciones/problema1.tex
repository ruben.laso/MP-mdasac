\section{Problema 1}
Ahora se define el siguiente problema
\begin{equation}\label{eq:problema1}
	\begin{cases}
	\epsilon y'' - x^2y' - y = 0, \quad & 0 < x < 1,
	\\
	y(0) = y(1) = 1, \quad & 0 < \epsilon \ll 1.
	\end{cases}
\end{equation}

Suponemos que existe una capa límite $x = 1$ y aplicamos la misma metodología que para el problema~\eqref{eq:friedrichs}.

Primero, calculamos los términos del desarrollo exterior. Comenzamos suponiendo
\begin{equation} \label{eq:problema1-out}
	y_{\text{out}}(x;\epsilon)
	=
	\sum_{n = 0}^{\infty} y_n(x) \epsilon^n
	=
	y_0(x) + y_1(x)\epsilon + y_2(x)\epsilon^2 + \dots
\end{equation}
y sustituimos~\eqref{eq:problema1-out} en~\eqref{eq:problema1}, de forma que nos queda
\begin{equation*}
	(y_0'' + \epsilon y_1 + \dots) + x^2(y_0' + \epsilon y_1 + \dots) - (y_0 + \epsilon y_1 + \dots) = 0.
\end{equation*}
Ahora separamos los términos de igual orden
\begin{align*}
	\bigo{1} &\implies y_0' + \frac{1}{x^2} y_0 = 0 \implies y_0 = c_0 e^{\frac{1}{x}},
	\\
	\bigo{\epsilon} &\implies y_1' + \frac{1}{x^2} y_1 = 0 \implies y_1 = c_1 e^{\frac{1}{x}},
	\\
	\vdots
\end{align*}
donde no podemos aplicar las condiciones de contorno para identificar el valor de las constantes $c_0$ y $c_1$.

Ahora proponemos el cambio de variable $X = \frac{1-x}{\epsilon}$ y suponemos un ancho de la capa límite $\delta(\epsilon) = \epsilon$. Con el cambio de variable obtenemos la ecuación
\begin{equation*}
	Y'' + (1 - \epsilon X)^2 Y' - \epsilon Y = 0.
\end{equation*}
Con ello podemos aplicar la misma técnica que para el desarrollo exterior
\begin{align*}
	\bigo{1}
	&\implies
	\begin{cases}
	Y_0'' + Y_0' = 0
	\\
	Y_0(X = 0) = y(x=1) 1
	\end{cases}
	\implies
	Y_0(X) = A + (1-A) e^{-X},
	\\
	\bigo{\epsilon}
	&\implies
	\begin{cases}
	Y_1'' + Y_1' = Y_0 + 2XY_0,
	\end{cases}
\end{align*}
donde aún debemos resolver las constantes.
Aplicando límites solamente se llega a que $c_0 e = A$. Por ello, suponemos otra capa límite en $x=0$ de anchura desconocida. Escalamos $x = \delta\hat{X}$, con $\hat{X} = \bigo{1}$. Así tenemos
\begin{equation*}
	\frac{\epsilon}{\delta^2} Y_{\hat{X}\hat{X}} - \delta\hat{X}^2Y_{\hat{X}} - Y = 0
\end{equation*}
y utilizamos balances dominantes
\begin{itemize}
	\item Balance $\frac{\epsilon}{\delta^2} Y_{\hat{X}\hat{X}} \sim \delta\hat{X}Y_{\hat{X}}$ y dominan:
	\begin{equation*}
		\frac{\epsilon}{\delta^2} = \delta
		\implies 
		\delta = \epsilon^{\frac{1}{3}} 
		\implies 
		\epsilon^{\frac{1}{3}} Y_{\hat{X}\hat{X}} - \epsilon^{\frac{1}{3}} \hat{X}^2 Y_{\hat{X}} - Y = 0.
	\end{equation*}
	Este desarrollo es inconsistente, pues domina $Y$.
	
	\item Balance $\delta\hat{X}^2Y_{\hat{X}} \sim Y$ y dominan: consistente, $\delta=1$, límite exterior.
	
	\item Balance $\frac{\epsilon}{\delta^2} Y_{\hat{X}\hat{X}} \sim Y$ y dominan:
	\begin{equation*}
		\frac{\epsilon}{\delta^2} = 1 \implies \delta(\epsilon) = \sqrt{\epsilon}.
	\end{equation*}
	Consistente. Tenemos que la capa límite asociada a $x=0$ tiene anchura $\delta(\epsilon) = \sqrt{\epsilon}$ y la variable de orden unidad $\hat{X} = \frac{x}{\sqrt{\epsilon}}$.
\end{itemize}

Con la nueva variable tenemos la ecuación
\begin{equation*}
	(F_0'' + \sqrt{\epsilon}F_1'' + \dots) - \frac{x^2}{\sqrt{\epsilon}} (F_0' + \sqrt{\epsilon} F_1' + \dots) - (F_0 + \sqrt{\epsilon} F_1 + \dots) = 0,
\end{equation*}
en la que podemos separar los términos de distinto orden
\begin{align*}
	\bigo{1}
	&\implies
	\begin{cases}
	F_0'' - F_0 = 0
	\\
	F_0(\hat{X}=0) = y(x=0) = 1
	\end{cases}
	\implies
	F_0(\hat{X}) = D_0 e^{-\hat{X}} + (1-D_0)e^{-\hat{X}},
	\\
	\bigo{\sqrt{\epsilon}}
	&\implies
	\begin{cases}
	F_1'' - F_1 = 0
	F_1(\hat{X}) = 0
	\end{cases}
	\implies
	F_1(\hat{X}) = D_1 \sin(\hat{X}) = 0,
	\\
	\vdots
\end{align*}

Si aplicamos límites de la solución exterior e interior tenemos que hay \textit{matching} si, y solo si, $c_0 = D_0 = 0$.

Finalmente, tenemos que
\begin{align*}
	y_n(x) &= c_n e^{\frac{1}{x}} = 0, \quad n = 0, 1, \dots
	\\
	Y_0\left(\frac{1-x}{\epsilon}\right) &= e^{\frac{x-1}{\epsilon}},
	\\
	Y_1\left(\frac{1-x}{\epsilon}\right) &= e^{\frac{x-1}{\epsilon}} \left(\frac{1 + x^2 - 2x}{\epsilon^2} + \frac{1-x}{\epsilon}\right),
	\\
	F_0\left(\frac{x}{\sqrt{\epsilon}}\right) &= e^{\frac{-x}{\sqrt{\epsilon}}},
	\\
	F_n\left(\frac{x}{\sqrt{\epsilon}}\right) &= 0, \quad n = 1, 2, \dots
	\\
	\mathcal{C}_n(x) &= 0, \quad n = 0, 1, \dots
\end{align*}
Por lo que la solución \textit{leading order} es
\begin{equation}\label{eq:problema-lo}
	y_{\text{unif}} (x;\epsilon) \sim e^{\frac{x-1}{\epsilon}} + e^{-\frac{x}{\sqrt{\epsilon}}},
\end{equation}
y la \textit{first order} es
\begin{equation}\label{eq:problema1-fo}
	y_{\text{unif}} (x;\epsilon) \sim e^{\frac{x-1}{\epsilon}} + e^{\frac{x-1}{\epsilon}} \left(\frac{1 + x^2 - 2x}{\epsilon^2} + \frac{1-x}{\epsilon}\right) + e^{-\frac{x}{\sqrt{\epsilon}}}.
\end{equation}

\subsection{Estudio de las soluciones asintóticas}
Para este problema, a falta de una solución analítica, hemos buscado una resolución numérica mediante el comando \texttt{bvp4c} de MATLAB. Para los resultados aquí mostrados se han utilizado los valores $\epsilon = \{0.01, 0.05, 0.1\}$. Las gráficas correspondientes se muestran en la \figurename{~\ref{fig:problema1}}.
\begin{figure}[htbp]
	\centering
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/problema1/lo_001}
		\caption{\textit{Leading order}, $\epsilon = 0.01$.}
		\label{fig:problema1-lo-001}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/problema1/fo_001}
		\caption{\textit{First order}, $\epsilon = 0.01$.}
		\label{fig:problema1-fo-001}
	\end{subfigure}%
	\\
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/problema1/lo_005}
		\caption{\textit{Leading order}, $\epsilon = 0.05$.}
		\label{fig:problema1-lo-005}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/problema1/fo_005}
		\caption{\textit{First order}, $\epsilon = 0.05$.}
		\label{fig:problema1-fo-005}
	\end{subfigure}%
	\\
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/problema1/lo_01}
		\caption{\textit{Leading order}, $\epsilon = 0.1$.}
		\label{fig:problema1-lo-01}
	\end{subfigure}%
	\begin{subfigure}[b]{0.49\linewidth}
		\centering\includegraphics[width=\textwidth]{figuras/problema1/fo_01}
		\caption{\textit{First order}, $\epsilon = 0.1$.}
		\label{fig:problema1-fo-01}
	\end{subfigure}%
	\caption{Comparativa de la solución exacta y las aproximaciones asintóticas para diferentes valores de $\epsilon$. Con líneas punteadas verticales se denotan los límites de las capas límite.}
	\label{fig:problema1}
\end{figure}

De estas representaciones de las funciones, podemos destacar dos cosas. Por un lado, se debe mencionar que las aproximaciones no son tan precisas con las realizadas para el problema de Friedrichs. Por otro, vemos que la mejora al añadir un término a la sucesión es relativamente pequeño y, a simple vista, poco relevante.

Sin embargo, si calculamos el error máximo obtenido por ambas soluciones para, por ejemplo, $\epsilon = 0.1$, tenemos que la solución \textit{leading order} comete un error del 51\%, mientras que la \textit{first order} falla por un 35\%. Estas cifras en el error son notablemente altas y, como se puede ver en la \figurename{~\ref{fig:problema1-error}}, tiene el mínimo alrededor de $\epsilon \approx 0.27$ para la solución \textit{leading order}, y en $\epsilon \approx 0.17$ para la \textit{first order}. En la primera mitad de la gráfica, mientras $\epsilon$ se mantiene pequeño, la aproximación \textit{first order} es la más precisa, y disminuye rápidamente a medida que crece $\epsilon$. En la segunda mitad de la gráfica, podemos ver que la \textit{leading order} se comporta mejor antes valores ``muy grandes'' de $\epsilon$.

Esto es debido a los pequeños valores que toma la función, que hace que el error relativo se dispare muy rápidamente aunque la función se aproxime mejor en términos absolutos.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/problema1/error}
	\caption{Error máximo relativo cometido en las aproximaciones.}
	\label{fig:problema1-error}
\end{figure}
