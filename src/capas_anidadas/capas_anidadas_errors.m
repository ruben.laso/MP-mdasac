function [ max_lo, max_fo ] = capas_anidadas_error( epsis )
% capas_anidadas_error: function to plot the errors in the approximations of
% the asymptotical approximations.
% It uses leading order.
% First order approximation has issues for this problem.

close all;
for i=1:length(epsis)
    error_lo(i) = capas_anidadas_lo(epsis(i));
    % error_fo(i) = capas_anidadas_fo(epsis(i));
end
close all;

p(1) = plot(epsis, error_lo, 'DisplayName', 'leading order');
hold on;
% p(2) = plot(epsis, error_fo, 'DisplayName', 'first order');

xlabel('\epsilon');
ylabel('max. error');

legend('show');
set(findall(gca, 'Type', 'Line'), 'LineWidth', 2);
lgd = legend(p, 'Location','northeast');
lgd.FontSize = 12;

max_lo = max(error_lo);
% max_fo = max(error_fo);

end  % friedrich
