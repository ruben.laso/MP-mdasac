function [ max_error ] = capas_anidadas_fo( epsi )
% capas_anidadas_lo: representation of the first order solutions the problem with nested limit layers.
% FIXME: the approximation is really bad. Has calculation errors.

close all
x = linspace(0.00001,0.99999,1000);

% numerical MATLAB solution
M = @(x,y) [y(2); -x.^2/epsi^3*y(2) + 1/epsi^3.*(x.^3+epsi)*y(1)];
bc = @(ya,yb) [ya(1)-1; yb(1)-exp(1/2)];
solinit = bvpinit(linspace(0, 1, 20), [1 1]);
sol = bvp4c(M, bc, solinit);
y = deval(sol,x);
y = y(1,:); % two solutions are given by the solver
% plot exact solution
legend_str = sprintf('numerical, \\epsilon = %g', epsi);
p(1) = plot(x, y(1,:), 'DisplayName', legend_str);
hold on;

% Compute and plot asymptotic solutions
% outer solution
y0 = @(x) exp(x.^2./2);
y1 = @(x) (1 - 1./x) .* exp(x.^2./2);
% inner solutions
Y0 = @(x) exp(-epsi./x);
Y1 = @(x) (1 + 2*epsi^3./(3.*x.^3) - epsi^4./(4.*x.^4)) .* exp(-epsi./x);
F0 = @(x) 2.*sqrt(x./epsi^2).*besselk(1,2.*sqrt(x./epsi^2));
F1 = @(x) 0.*x;
% common solution
C1 = @(x) exp(1/2) - 1;
% leading order terms (epsilon^0)
y_out = @(x) y0(x) + epsi.*y1(x);
y_inn = @(x) Y0(x) + epsi.*Y1(x) + F0(x) + epsi*F1(x);
y_com = @(x) C1(x);
y_unif = @(x) y_out(x) + y_inn(x) - y_com(x);

% plot leading order solution
legend_str = sprintf('O(\\epsilon), \\epsilon = %g', epsi);
p(2) = plot(x, y_unif(x), 'DisplayName', legend_str);
hold on;

% plot inner and outer solutions
legend_str = sprintf('outer, \\epsilon = %g', epsi);
p(3) = plot(x, y_out(x), '--', 'DisplayName', legend_str);
legend_str = sprintf('inner 1, \\epsilon = %g', epsi);
p(4) = plot(x, F0(x) + epsi*F1(x), '--', 'DisplayName', legend_str);
legend_str = sprintf('inner 2, \\epsilon = %g', epsi);
p(5) = plot(x, Y0(x) + epsi.*Y1(x), '--', 'DisplayName', legend_str);

% plot matching layer bounds
layer_thickness = epsi;
min_bound = 0;
max_bound = min_bound + layer_thickness;
plot([min_bound min_bound], ylim, ':k');
plot([max_bound max_bound], ylim, ':k');

layer_thickness = epsi^2;
min_bound = 0;
max_bound = min_bound + layer_thickness;
plot([min_bound min_bound], ylim, ':k');
plot([max_bound max_bound], ylim, ':k');

% legend and axis attributes
legend('show');
set(findall(gca, 'Type', 'Line'), 'LineWidth', 2);
lgd = legend(p, 'Location','northeast');
lgd.FontSize = 20;

xlabel('x');
ylabel('y(x)');

% compute relative error
x = linspace(0.0001, 1, 1000);
max_error = max( abs(y - y_unif(x)) ./ y);

end  % capas_anidadas_lo
