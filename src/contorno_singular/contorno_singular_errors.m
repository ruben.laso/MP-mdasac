function [ max_lo, max_fo ] = contorno_singular_errors( epsis )
% contorno_singular_errors: function to plot the errors in the approximations of
% the asymptotical approximations.
% It uses leading order and first order approximations.

close all;
for i=1:length(epsis)
    error_lo(i) = contorno_singular_lo(epsis(i));
    error_fo(i) = contorno_singular_fo(epsis(i));
end
close all;

p(1) = plot(epsis, error_lo, 'DisplayName', 'leading order');
hold on;
p(2) = plot(epsis, error_fo, 'DisplayName', 'first order');

xlabel('\epsilon');
ylabel('max. error');

legend('show');
set(findall(gca, 'Type', 'Line'), 'LineWidth', 2);
lgd = legend(p, 'Location','northwest');
lgd.FontSize = 12;

max_lo = max(error_lo);
max_fo = max(error_fo);

end  % contorno_singular_errors
