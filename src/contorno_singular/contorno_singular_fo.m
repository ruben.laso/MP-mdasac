function [ max_error ] = contorno_singular_fo( epsi )
% contorno_singular_fo: representation of the first order solutions for the singular bound problem.

close all
x = linspace(0.0001, 0.9999, 1000);

% numerical MATLAB solution
M = @(x,y) [y(2); -1/(epsi*x)*y(2) - 1/epsi*y(1)];
bc = @(ya,yb) [ya(1)-(1-epsi/4); yb(1)-exp(-1/2)];
solinit = bvpinit(linspace(0.0001, 0.9999, 20), [1 1]);
sol = bvp4c(M, bc, solinit);
y = deval(sol,x);
y = y(1,:); % two solutions are given by the solver
% plot exact solution
legend_str = sprintf('numerical, \\epsilon = %g', epsi);
p(1) = plot(x, y(1,:), 'DisplayName', legend_str);
hold on;

% Compute and plot asymptotic solutions
% outer solution
y0 = @(x) exp(-x.^2/2);
y1 = @(x) -1/4*(x.^2 - 1).^2 .* exp(-x.^2/2);
% inner solutions
Y0 = @(x) x.*0;
% common solution
C0 = @(x) x.*0;
% leading order terms (epsilon^0)
y_out = @(x) y0(x) + epsi*y1(x);
y_inn = @(x) Y0(x);
y_com = @(x) C0(x);
y_unif = @(x) y_out(x) + y_inn(x) - y_com(x);

% plot leading order solution
legend_str = sprintf('O(\\epsilon), \\epsilon = %g', epsi);
p(2) = plot(x, y_unif(x), 'DisplayName', legend_str);
hold on;

% legend and axis attributes
legend('show');
set(findall(gca, 'Type', 'Line'), 'LineWidth', 2);
lgd = legend(p, 'Location','southeast');
lgd.FontSize = 20;

xlabel('x');
ylabel('y(x)');
ylim([0 1]);

% compute relative error
x = linspace(0.0001, 1, 1000);
max_error = max( abs(y - y_unif(x)) ./ y);

end  % contorno_singular_fo
