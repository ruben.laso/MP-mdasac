function [ max_error ] = friedrichs_fo( epsi )
% friedrichs: representation of the solutions for the Friedichs problem.
close all

x = linspace(0,1,1000);

% exact solution
lambdaP = (-1 + sqrt(1+epsi)) ./ epsi;
lambdaM = (-1 - sqrt(1+epsi)) ./ epsi;
exact = @(x) (exp(x.*lambdaP) - exp(x.*lambdaM)) / (exp(lambdaP) - exp(lambdaM));
% plot exact solution
legend_str = sprintf('exact, \\epsilon = %g', epsi);
p(1) = plot(x, exact(x), 'DisplayName', legend_str);
hold on;

% Compute and plot asymptotic solutions
% outer solutions
y0 = @(x) exp(0.5.*(x-1));
y1 = @(x) (1-x)./8 .* exp(0.5.*(x-1));
% inner solutions
Y0 = @(x) exp(-0.5) .* (1 - exp(-2.*x./epsi));
Y1 = @(x) x./(2.*epsi) .* exp(-0.5) .* (1 + exp(-2.*x./epsi)) + exp(-0.5)/8 .* (1 - exp(-2.*x./epsi));
% common solutions
C0 = @(x) exp(-0.5);
C1 = @(x) exp(-0.5) .* (1 + epsi./8 + x./2);
% first order terms (epsilon^1)
y_out = @(x) y0(x) + epsi.*y1(x);
y_inn = @(x) Y0(x) + epsi.*Y1(x);
y_com = @(x) C1(x);
y_unif = @(x) y_inn(x) + y_out(x) - y_com(x);

% plot leading order solution
legend_str = sprintf('O(\\epsilon), \\epsilon = %g', epsi);
p(2) = plot(x, y_unif(x), 'DisplayName', legend_str);
hold on;

% plot inner and outer solutions
legend_str = sprintf('outer, \\epsilon = %g', epsi);
p(3) = plot(x, y_out(x), '--', 'DisplayName', legend_str);

legend_str = sprintf('inner, \\epsilon = %g', epsi);
p(4) = plot(x, y_inn(x), '--', 'DisplayName', legend_str);

% plot matching layer bounds
layer_thickness = epsi;
min_bound = 0;
max_bound = min_bound + layer_thickness;
plot([min_bound min_bound], ylim, ':k');
plot([max_bound max_bound], ylim, ':k');

legend('show');
set(findall(gca, 'Type', 'Line'), 'LineWidth', 2);
lgd = legend(p, 'Location','southeast');
lgd.FontSize = 20;

xlabel('x');
ylabel('y(x)');

% compute relative error
x = linspace(0.0001, 1, 1000);
max_error = max( abs(exact(x) - y_unif(x)) ./ exact(x));

end  % friedrichs
