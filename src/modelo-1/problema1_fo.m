function [ max_error ] = problema1_fo( epsi )
% problema1_lo: representation of the first order solutions for the problem 1.

close all
x = linspace(0.00001,0.99999,1000);

% numerical MATLAB solution
M = @(x,y) [y(2); -x.^2./epsi .*y(2) + 1/epsi.*y(1)];
bc = @(ya,yb) [ya(1)-1; yb(1)-1];
solinit = bvpinit(linspace(0, 1, 200), [1 1]);
sol = bvp4c(M, bc, solinit);
y = deval(sol,x);
y = y(1,:); % two solutions are given by the solver
% plot exact solution
legend_str = sprintf('numerical, \\epsilon = %g', epsi);
p(1) = plot(x, y(1,:), 'DisplayName', legend_str);
hold on;

% Compute and plot asymptotic solutions
% outer solution
y0 = @(x) exp(1-1./x);
y1 = @(x) (1/5.*x.^(-5) - 1/2.*x.^(-4) + 3/10) .* exp(1-1./x);
% inner solutions
Y0 = @(x) exp(-x./sqrt(epsi));
Y1 = @(x) (-1/6.*(x./sqrt(epsi)).^3 - 1/4.*(x./sqrt(epsi)).^2 - 1/4.*(x./sqrt(epsi))) .* exp(-x./sqrt(epsi));
Y2 = @(x) (1/72.*(x./sqrt(epsi)).^6 + 1/60.*(x./sqrt(epsi)).^5 + 1/96.*(x./sqrt(epsi)).^4 ...
            - 1/48.*(x./sqrt(epsi)).^3 - 1/32.*(x./sqrt(epsi)).^2 - 1/32.*(x./sqrt(epsi))) .* exp(-x./sqrt(epsi));
% common solution
C0 = @(x) x.*0;
% leading order terms (epsilon^0)
y_out = @(x) y0(x) + epsi*y1(x);
y_inn = @(x) Y0(x) + sqrt(epsi)*Y1(x) + epsi*Y2(x);
y_com = @(x) C0(x);
y_unif = @(x) y_out(x) + y_inn(x) - y_com(x);

% plot leading order solution
legend_str = sprintf('O(\\epsilon), \\epsilon = %g', epsi);
p(2) = plot(x, y_unif(x), 'DisplayName', legend_str);
hold on;

% plot inner and outer solutions
legend_str = sprintf('outer, \\epsilon = %g', epsi);
p(3) = plot(x, y_out(x), '--', 'DisplayName', legend_str);
legend_str = sprintf('inner, \\epsilon = %g', epsi);
p(4) = plot(x, y_inn(x), '--', 'DisplayName', legend_str);

% plot matching layer bounds
layer_thickness = sqrt(epsi);
min_bound = 0;
max_bound = min_bound + layer_thickness;
plot([min_bound min_bound], ylim, ':k');
plot([max_bound max_bound], ylim, ':k');

% legend and axis attributes
legend('show');
set(findall(gca, 'Type', 'Line'), 'LineWidth', 2);
lgd = legend(p, 'Location','southeast');
lgd.FontSize = 20;

xlabel('x');
ylabel('y(x)');

% compute relative error
x = linspace(0.0001, 1, 1000);
max_error = max( abs(y - y_unif(x)) ./ y);

end  % problema1_lo
