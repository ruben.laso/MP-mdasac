function [ max_error ] = problema1_fo( epsi )
% problema1_lo: representation of the first order solutions for the problem 1.

close all
x = linspace(0.000001,0.99999,1000);

% analytical solution
exact = @(x) x - log(1 + x./epsi) / (log(epsi) - log(1+epsi));
% plot exact solution
legend_str = sprintf('exact, \\epsilon = %g', epsi);
p(1) = plot(x, exact(x), 'DisplayName', legend_str);
hold on;

% Compute and plot asymptotic solutions
nu0 = -1/log(epsi);
y_out = @(x) 1 + x + nu0.*log(x) + epsi*nu0^2.*(-log(x)) + epsi*nu0.*(1./x - 1);
y_inn = @(x) nu0.*log(1 + x./epsi) + x + epsi*nu0^2.*(-log(1 + x./epsi));
y_com = @(x) 0.*x;
y_unif = @(x) y_out(x) + y_inn(x) - y_com(x);
y_unif = @(x) x + (-1/log(epsi) - epsi/(log(epsi)^2) + epsi^2/(2*log(epsi)^2)) .* log(1 + x./epsi);

% plot leading order solution
legend_str = sprintf('O(\\epsilon), \\epsilon = %g', epsi);
p(2) = plot(x, y_unif(x), 'DisplayName', legend_str);
hold on;

% plot inner and outer solutions
legend_str = sprintf('outer, \\epsilon = %g', epsi);
p(3) = plot(x, y_out(x), '--', 'DisplayName', legend_str);
legend_str = sprintf('inner, \\epsilon = %g', epsi);
p(4) = plot(x, y_inn(x), '--', 'DisplayName', legend_str);

% plot matching layer bounds
layer_thickness = epsi;
min_bound = 0;
max_bound = min_bound + layer_thickness;
plot([min_bound min_bound], ylim, ':k');
plot([max_bound max_bound], ylim, ':k');

% legend and axis attributes
legend('show');
set(findall(gca, 'Type', 'Line'), 'LineWidth', 2);
lgd = legend(p, 'Location','southeast');
lgd.FontSize = 20;

ylim([0 2.5]);

xlabel('x');
ylabel('y(x)');

% compute relative error
x = linspace(0.0001, 1, 1000);
max_error = max( abs(exact(x) - y_unif(x)) ./ exact(x));

end  % problema1_lo
