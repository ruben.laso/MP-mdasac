function [ max_error ] = problema2_lo( epsi )
% problema2_lo: representation of the leading order solutions for the problem 2.

close all
x = linspace(0.00001,0.99999,1000);

% analytical solution
exact = @(x) x - log(1 + x./epsi) / (log(epsi) - log(1+epsi));
% plot exact solution
legend_str = sprintf('exact, \\epsilon = %g', epsi);
p(1) = plot(x, exact(x), 'DisplayName', legend_str);
hold on;

% Compute and plot asymptotic solutions
% outer solution
y0 = @(x) 1 + x;
% inner solutions
nu0 = -1/log(epsi);
Y0 = @(x) nu0.*log(1 + x./epsi);
% common solution
C0 = @(x) x.*0 + 1;
% leading order terms (epsilon^0)
y_out = @(x) y0(x);
y_inn = @(x) Y0(x);
y_com = @(x) C0(x);
y_unif = @(x) y_out(x) + y_inn(x) - y_com(x);

% plot leading order solution
legend_str = sprintf('O(1), \\epsilon = %g', epsi);
p(2) = plot(x, y_unif(x), 'DisplayName', legend_str);
hold on;

% plot inner and outer solutions
legend_str = sprintf('outer, \\epsilon = %g', epsi);
p(3) = plot(x, y_out(x), '--', 'DisplayName', legend_str);
legend_str = sprintf('inner, \\epsilon = %g', epsi);
p(4) = plot(x, y_inn(x), '--', 'DisplayName', legend_str);

% plot matching layer bounds
layer_thickness = epsi;
min_bound = 0;
max_bound = min_bound + layer_thickness;
plot([min_bound min_bound], ylim, ':k');
plot([max_bound max_bound], ylim, ':k');

% legend and axis attributes
legend('show');
set(findall(gca, 'Type', 'Line'), 'LineWidth', 2);
lgd = legend(p, 'Location','southeast');
lgd.FontSize = 20;

xlabel('x');
ylabel('y(x)');

% compute relative error
x = linspace(0.0001, 1, 1000);
max_error = max( abs(exact(x) - y_unif(x)) ./ exact(x));

end  % problema2_lo
