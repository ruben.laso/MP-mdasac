function [ max_error ] = problema1_lo( epsi )
% problema1_lo: representation of the leading order solutions for the problem 1.

close all
x = linspace(0,1,1000);

% numerical MATLAB solution
M = @(x,y) [y(2); x.^2./epsi .*y(2) + 1/epsi.*y(1)];
bc = @(ya,yb) [ya(1)-1; yb(1)-1];
solinit = bvpinit(linspace(0, 1, 20), [1 1]);
sol = bvp4c(M, bc, solinit);
y = deval(sol,x);
y = y(1,:); % two solutions are given by the solver
% plot exact solution
legend_str = sprintf('numerical, \\epsilon = %g', epsi);
p(1) = plot(x, y(1,:), 'DisplayName', legend_str);
hold on;

% Compute and plot asymptotic solutions
% outer solution
y0 = @(x) x.*0;
% inner solutions
Y0 = @(x) exp((x-1)/epsi);
F0 = @(x) exp(-x/sqrt(epsi));
% common solution
C0 = @(x) x.*0;
% leading order terms (epsilon^0)
y_out = @(x) y0(x);
y_inn = @(x) Y0(x) + F0(x);
y_com = @(x) C0(x);
y_unif = @(x) y_out(x) + y_inn(x) - y_com(x);

% plot leading order solution
legend_str = sprintf('O(1), \\epsilon = %g', epsi);
p(2) = plot(x, y_unif(x), 'DisplayName', legend_str);
hold on;

% plot inner and outer solutions
legend_str = sprintf('outer, \\epsilon = %g', epsi);
p(3) = plot(x, y_out(x), '--', 'DisplayName', legend_str);
legend_str = sprintf('inner x=0, \\epsilon = %g', epsi);
p(4) = plot(x, F0(x), '--', 'DisplayName', legend_str);
legend_str = sprintf('inner x=1, \\epsilon = %g', epsi);
p(5) = plot(x, Y0(x), '--', 'DisplayName', legend_str);

% plot matching layer bounds
layer_thickness = sqrt(epsi);
min_bound = 0;
max_bound = min_bound + layer_thickness;
plot([min_bound min_bound], ylim, ':k');
plot([max_bound max_bound], ylim, ':k');

layer_thickness = epsi;
max_bound = 1;
min_bound = max_bound - layer_thickness;
plot([min_bound min_bound], ylim, ':k');
plot([max_bound max_bound], ylim, ':k');

% legend and axis attributes
legend('show');
set(findall(gca, 'Type', 'Line'), 'LineWidth', 2);
lgd = legend(p, 'Location','northeast');
lgd.FontSize = 20;

xlabel('x');
ylabel('y(x)');

% compute relative error
x = linspace(0.0001, 1, 1000);
max_error = max( abs(y - y_unif(x)) ./ y);

end  % problema1_lo
